# Minecraft City Builder

Ce projet à pour but de faciliter la création de cité pour minecraft.

Le serveur se base actuellement sur l'architecture Paper sur la version 1.17. Tout l'environnement est dockerisé pour permettre une plus grande réusabilité du projet.

Voici la liste des plugins installés:
 
* [ActionBarHealth](https://www.spigotmc.org/resources/action-bar-health.2661/)
* [AdvancedAchievements](https://www.spigotmc.org/resources/advanced-achievements.83466/)
* [AnimatedScoreboard](https://www.spigotmc.org/resources/animatedscoreboard.20848/)
* [AntiCooldown](https://www.spigotmc.org/resources/%E2%AD%90v4-release%E2%AD%90anticooldown-1-9-1-17-1.51321/)
* [AnotherDailyBonus](https://www.spigotmc.org/resources/anotherdailybonus-%E2%80%A2-increase-player-retention-1-15-1-17.79674/)
* [AuctionHouse](https://www.spigotmc.org/resources/auctionhouse.61836/)
* [BetterEnderChest](https://www.spigotmc.org/resources/betterenderchest.2073/)
* [BetterTeams](https://www.spigotmc.org/resources/better-teams.17129/)
* [Chunky](https://www.spigotmc.org/resources/chunky.81534/)
* [Citizen](https://www.spigotmc.org/resources/citizens.13811/)
* [ConsoleScheduler](https://dev.bukkit.org/projects/consolescheduler-reloaded)
* [CoreProtect](https://www.spigotmc.org/resources/coreprotect.8631/)
* [CrateReloaded](https://www.spigotmc.org/resources/free-crate-reloaded-mystery-crate-1-8-1-16-x.861/)
* [DropHeads](https://dev.bukkit.org/projects/dropheads)
* [EconomyShopGUI](https://www.spigotmc.org/resources/economyshopgui.69927/)
* [EnhancedEnchants](https://www.spigotmc.org/resources/enhancedenchants.87089/)
* [EssentialsX](https://www.spigotmc.org/resources/essentialsx.9089/)
* [EssentialsXSpawn](https://www.spigotmc.org/resources/essentialsx.9089/)
* [ExtraContexts](https://github.com/LuckPerms/ExtraContexts)
* [FAWE](https://www.spigotmc.org/resources/fast-async-worldedit.13932/)
* [GSlotMachine](https://www.spigotmc.org/resources/gslotmachine.55107/)
* [HolographicDisplays](https://dev.bukkit.org/projects/holographic-displays)
* [HolographicPlaceholders](https://www.spigotmc.org/resources/holographic-placeholders-holoextension.64535/)
* [LuckPerms](https://www.spigotmc.org/resources/luckperms.28140/)
* [MoneyFromMobs](https://www.spigotmc.org/resources/money-from-mobs-1-12-1-17.79137/)
* [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/)
* [PlayedTime](https://www.spigotmc.org/resources/playtime.26016/)
* [ProtocolLib](https://www.spigotmc.org/resources/protocollib.1997/)
* [TabList](https://www.spigotmc.org/resources/animated-tab-tablist.46229/)
* [TouchscreenHolograms](https://dev.bukkit.org/projects/touchscreen-holograms)
* [TreeGravity](https://www.spigotmc.org/resources/1-17-treegravity-tree-feller.59283/)
* [Vault](https://www.spigotmc.org/resources/vault.34315/)
* [WildChests](https://bg-software.com/wildchests/)
* [WildStacker](https://bg-software.com/wildstacker/)
* [WildTools](https://bg-software.com/wildtools/)
* [WorldGuard](https://dev.bukkit.org/projects/worldguard)
* [WorldManager](https://www.spigotmc.org/resources/%E2%AD%90-worldmanager-%E2%AD%90.96182/)

## Installation

Démarrer le serveur:

```shell
$ make deploy
```

Fermer le serveur:

```shell
$ make undeploy
```

Ouvrir la console:

```shell
$ make console
```

Consulter les logs:

```shell
$ make logs
```

## Initialiser une cité

#### Avant le début de la cité

1 - Modifier le motd dans le fichier [server.properties](./paper/server.properties) et le nom du serveur dans [PlaceholderAPI](paper/plugins/PlaceholderAPI/config.yml)  
2 - Construire/remplacer les cartes du serveur (world, world_nether, world_the_end)  
3 - Prégénérer les chunks et la world border vanilla (exemple avec un radius de 1000 => carte en 2000x2000)

```
$ make console
> worldborder center 0 0
> worldborder set 1000
> chunky worldborder
> chunky start
```

4 - Définir le point de spawn sur la carte

* /setworldspawn ~ ~ ~
* /gamerule spawnRadius 0

5 - Sécuriser la cité à l'aide d'une region WorldGuard

* Sélectionner une zone avec le //wand 
* //expand vert
* /region define city
* Ajouter les flags suivants sur la region worldguard dans les fichiers de configuration `(plugins/WorldGuard/worlds/world/regions.yml)`

```yml
flags: {other-explosion: deny, deny-message: 'Désolé, tu n''es pas autorisé
                à faire ça ici.', frosted-ice-form: deny, lava-fire: deny, use: deny,
            greeting: '&aVous sentez l''énergie de la cité vous apaiser.', leaf-decay: deny,
            ice-melt: deny, block-trampling: deny, snow-fall: deny, interact: allow,
            chest-access: allow, firework-damage: deny, fire-spread: deny, enderdragon-block-damage: deny,
            sleep: allow, snowman-trails: deny, feed-delay: 5, mob-damage: deny, ravager-grief: deny,
            entity-painting-destroy: deny, mushroom-growth: allow, lightning: deny,
            wither-damage: deny, ice-form: deny, chorus-fruit-teleport: deny, feed-amount: 5,
            lighter: deny, enderman-grief: deny, pistons: deny, pvp: deny, mob-spawning: deny,
            enderpearl: deny, creeper-explosion: deny, vine-growth: deny, item-frame-rotation: deny,
            potion-splash: deny, damage-animals: deny, frosted-ice-melt: deny, snow-melt: deny,
            tnt: deny, ghast-fireball: deny, entity-item-frame-destroy: deny, allow-shop: allow,
            farewell: '&cLa cité n''a plus aucun effet de protection sur vous.'}
```

6 - Ajouter les PNJ/Shops dans la cité

Voir [Shops](./docs/shops.md).

7 - Ajouter la banquier d'équipe

* Ajouter un PNJ avec Citizen
* /npc command add cp team-bank <p>

8 - Ajouter le NPC pour les bonus journaliers

* Ajouter un PNJ avec Citizen
* /npc trait daily-rewards

9 - Positioner les loot crates

* Poser un coffre
* /cr set [ANTIQUE, CELESTE, LEGENDAIRE]

10 - Créer les Slot Machine

Pour la slot machine des clés de lotterie:

* Créer une zone de 4x2 et ajouter un bouton (position 4)
* /machine create -machine:crate -type:crate
* /machine setbutton -machine:crate   (en ciblant le bouton, position 4)
* /machine setcase -machine:crate -case:1   (en ciblant le premier bloc, position 1)
* /machine setcase -machine:crate -case:2   (en ciblant le premier bloc, position 2)
* /machine setcase -machine:crate -case:3   (en ciblant le premier bloc, position 3)

Pour la slot machine d'argent:

* Créer une zone de 4x2 et ajouter un bouton (position 4)
* /machine create -machine:money -type:money
* /machine setbutton -machine:money   (en ciblant le bouton, position 4)
* /machine setcase -machine:money -case:1   (en ciblant le premier bloc, position 1)
* /machine setcase -machine:money -case:2   (en ciblant le premier bloc, position 2)
* /machine setcase -machine:money -case:3   (en ciblant le premier bloc, position 3)

11 - Ajouter les hologrammes dans la cité

Pour le score des équipes:
* /teama holo create score

Pour ranking de l'équipe la plus riche:
* /teama holo create money

Pour le ranking des joueurs KILLS/DEATH/PLAYTIME/MONEY:

* Ajouter dans le fichier de configuration `paper/plugins/HolographicDisplays/database.yml`

```yaml
KILL:
  location: world, 63.159, 155.200, 120.505 # A modifier
  lines:
  - '&e✦ &aTOP 5 KILLS &e✦'
  - '&e#1 - &f{hpe-statistic_player_kills-1-user} &e➤&a {hpe-statistic_player_kills-1-value}'
  - '&e#2 - &f{hpe-statistic_player_kills-2-user} &e➤&a {hpe-statistic_player_kills-2-value}'
  - '&e#3 - &f{hpe-statistic_player_kills-3-user} &e➤&a {hpe-statistic_player_kills-3-value}'
  - '&e#4 - &f{hpe-statistic_player_kills-4-user} &e➤&a {hpe-statistic_player_kills-4-value}'
  - '&e#5 - &f{hpe-statistic_player_kills-5-user} &e➤&a {hpe-statistic_player_kills-5-value}'
DEATH:
  location: world, 65.159, 155.200, 122.505 # A modifier
  lines:
  - '&e✦ &4TOP 5 MORTS &e✦'
  - '&e#1 - &f{hpe-statistic_deaths-1-user} &e➤&4 {hpe-statistic_deaths-1-value}'
  - '&e#2 - &f{hpe-statistic_deaths-2-user} &e➤&4 {hpe-statistic_deaths-2-value}'
  - '&e#3 - &f{hpe-statistic_deaths-3-user} &e➤&4 {hpe-statistic_deaths-3-value}'
  - '&e#4 - &f{hpe-statistic_deaths-4-user} &e➤&4 {hpe-statistic_deaths-4-value}'
  - '&e#5 - &f{hpe-statistic_deaths-5-user} &e➤&4 {hpe-statistic_deaths-5-value}'
PLAYTIME:
  location: world, 69.159, 155.200, 123.505 # A modifier
  lines:
  - '&e✦ &bTOP 5 TEMPS DE JEU &e✦'
  - '&e#1 - &f{hpe-statistic_seconds_played-1-user} &e➤&b {hpe-statistic_seconds_played-1-value}'
  - '&e#2 - &f{hpe-statistic_seconds_played-2-user} &e➤&b {hpe-statistic_seconds_played-2-value}'
  - '&e#3 - &f{hpe-statistic_seconds_played-3-user} &e➤&b {hpe-statistic_seconds_played-3-value}'
  - '&e#4 - &f{hpe-statistic_seconds_played-4-user} &e➤&b {hpe-statistic_seconds_played-4-value}'
  - '&e#5 - &f{hpe-statistic_seconds_played-5-user} &e➤&b {hpe-statistic_seconds_played-5-value}'
MONEY:
  location: world, 67.159, 155.200, 124.505 # A modifier
  lines:
  - '&e✦ &6TOP 5 YC &e✦'
  - '&e#1 - &f{hpe-baltop-1-user} &e➤&6 {hpe-baltop-1-value}'
  - '&e#2 - &f{hpe-baltop-2-user} &e➤&6 {hpe-baltop-2-value}'
  - '&e#3 - &f{hpe-baltop-3-user} &e➤&6 {hpe-baltop-3-value}'
  - '&e#4 - &f{hpe-baltop-4-user} &e➤&6 {hpe-baltop-4-value}'
  - '&e#5 - &f{hpe-baltop-5-user} &e➤&6 {hpe-baltop-5-value}'
```

12 - Modifier le livre de règles de la cité

* Création d'un livre avec l'outil https://minecraft.tools/en/book.php
* Surcharger le kit new_player: /kitcreate new_player

13 - Ajouter des coffres (secrets) dans la cité

14 - Réinitiliser toutes les statistiques des joueurs

* Fermer le serveur
* Supprimer les bases de données dans [AdvancedAchievements](./paper/plugins/AdvancedAchievements)
* Supprimer la base de donnée `playerdata-sqlite.db` dans [AnimatedScoreboard](.paper/plugins/AnimatedScoreboard)
* Supprimer la base de donnée `auctions.db` dans [AuctionHouse](.paper/plugins/AuctionHouse)
* Supprimer le dossier `chestData` dans [BetterEnderChest](.paper/plugins/BetterEnderChest)
* Supprimer les dossiers `teamInfo` et le fichier `teams.yml` dans [BetterTeams](.paper/plugins/BetterTeams)
* Supprimer la base de données `database.db` dans [CoreProtect](.paper/plugins/CoreProtect)
* Supprimer le dossier `userdata` dans [Essentials](.paper/plugins/Essentials)
* Supprimer le dossier `yaml-storage/users` dans [LuckPerms](.paper/plugins/LuckPerms)
* Supprimer le fichier `userdata.json` dans [PlayTime](.paper/plugins/PlayTime)
* Supprimer le fichier `database.db` dans [WildChests](.paper/plugins/WildChests)
* Supprimer le fichier `database.db` dans [WildStacker](.paper/plugins/WildChests)
* Supprimer le dossier `playerdata` dans le dossier world
* Lancer le serveur et executer les commandes suivantes en jeu
* /advancement revoke @a everything
* /aach delete * @a
* /eco set * 0
* /teama disband *

_Note: il faudrait ajouter une commande qui automatise la supression des fichiers/dossiers._

#### Au début de la cité

1 - Création des équipes

* /teama create [NOM EQUIPE]
* /teama invite [JOUEUR] [NOM EQUIPE]

#### Pendant la cité

##### Events généraux

Multiplier les loot d'argent des mobs:

* /mfmevent start [POURCENTAGE] [HEURES] [MINUTES] [SECONDES]

Multiplier les prix de ventes au shop:

* Démarer le projet [Symfony](./web)
* Lancer la commande: make console command="app:economy-shop-guy:multiply-prices --factor=2"
* Lancer la commande: make console command="app:essential:create-worth-file"
* /essentials reload
* /sreload

#### Events journaliers

##### Mondes

Pour créer un monde custom il faut générer une nouvelle carte grâce à [WorldManager](https://www.spigotmc.org/resources/%E2%AD%90-worldmanager-%E2%AD%90.96182/):

* /wm create world_event [NORMAL|FLAT|LARGE_BIOMES|AMPLIFIED|NETHER|THE_END|EMPTY]
* /wm teleport world_event

##### Parkour

[Documentation](https://a5h73y.github.io/Parkour/)

Création du lobby:

* /wm create world_parkour [NORMAL|FLAT|LARGE_BIOMES|AMPLIFIED|NETHER|THE_END|EMPTY]
* /wm teleport world_parkour
* /pa setlobby

Création d'un parkour:

* /pa create [NOM_PARKOUR]
* /pa checkpoint [ORDRE] (à utiliser pour tout les checkpoints)
* /pa kit (et placer le finish bloc)
* /pa ready [NOM_PARKOUR]
* /pa done
* /pa test
* /pa settings [NOM_PARKOUR]

Création d'un hologram (leaderboard)

* /hd create parkour_NOM_PARKOUR TOP 3 - NOM_PARKOUR
* /hd addline parkour_NOM_PARKOUR {slow}%parkour_topten_NOM_PARKOUR_1%
* /hd addline parkour_NOM_PARKOUR {slow}%parkour_topten_NOM_PARKOUR_2%
* /hd addline parkour_NOM_PARKOUR {slow}%parkour_topten_NOM_PARKOUR_3%

Téléporter à une parkour:

* /pa challenge create [NOM_PARKOUR] [MONTANT_PARIÉ]
* /pa invite [JOUEUR_1] [JOUEUR_2] [JOUEUR_N]
* /pa challenge info
* /pa challenge begin


# TODO

#### Plugins

##### Configuration

- [ ] [CommandPanels](https://www.spigotmc.org/resources/command-panels-custom-guis.67788/)
    - [ ] Ajouter un panel pour les joueurs accessible par une commande (/infos)
        - [ ] Accès à la schedule des events
        - [ ] Accès au /modreq
    - [ ] Ajouter un panel pour améliorer le niveau de l'équipe (les niveaux peuvent octroyer des bonus, ex: prix plus intéressant sur certaines ressources)
- [ ] [EconomyShopGUI](https://www.spigotmc.org/resources/economyshopgui.69927/)
    - [ ] Installer la version Premium
      - [ ] Ajouter un PNJ qui achète/vends des objets à contenu limité et dont le stock est modifié tous les jours (https://gpplugins.gitbook.io/economyshopgui/basics/limited-stock)
    - [ ] Dispatcher le contenu de la section Others dans les autres sections (si possible)

##### Events

* Parkour

- [ ] [Parkour](https://www.spigotmc.org/resources/parkour.23685/)
- [ ] [ajParkour](https://www.spigotmc.org/resources/ajparkour-rewards-1-7-10-1-17-x-mysql-storage-option.60909/)
- [ ] [HubParkour](https://www.spigotmc.org/resources/hubparkour.47713/)

* BlockShuffle

- [ ] [BlockShuffle](https://www.spigotmc.org/resources/blockshuffle-1-13-x-1-17-x-minigame.81598/)

* Boost

- [ ] [Boost](https://www.spigotmc.org/resources/boost-unique-new-minigame-1-12-1-17-1.75950/)

* PvP

- [ ] [MultiArenaDuels](https://www.spigotmc.org/resources/multiarena-duels-1v1-open-source-leaderboard.44820/)

* Build 

- [ ] [Building Game](https://www.spigotmc.org/resources/building-game.10547/)
- [ ] [BuildBattle](https://www.spigotmc.org/resources/build-battle-guessthebuild-1-8-8-1-17.44703/)
* Manhunt

- [ ] [Manhunt](https://www.spigotmc.org/resources/manhunt.91326/)
- [ ] [Manhunt+](https://www.spigotmc.org/resources/manhunt-1-16x-1-17x.86708/)

* BlockBall

- [ ] [BlockBall](https://www.spigotmc.org/resources/blockball-minigame-bungeecord-soccer-football-1-8-1-17.15320/)

* Paintball

- [ ] [Paintball](https://www.spigotmc.org/resources/paintball-battle-team-minigame-1-8-1-17.76676/)

* Hunger Games

- [ ] [HungerGames](https://www.spigotmc.org/resources/hungergames.65942/)
- [ ] [UltimateHungerGames](https://www.spigotmc.org/resources/ultimatehungergames.88705/)

* TNT Run

- [TNTRun_reloaded](https://www.spigotmc.org/resources/tntrun_reloaded-tntrun-for-1-13-1-17.53359/)

* King of The Ladder

- [ ] [King of the Ladder](https://www.spigotmc.org/resources/king-of-the-ladder.80686/)

* Villager Defence

- [ ] [VillagerDefence](https://www.spigotmc.org/resources/villager-defense.90055/)
- [ ] [VillageDefence](https://www.spigotmc.org/resources/village-defense-1-8-8-1-17.41869/)

* Autre/Fun

- [ ] [TerminatorAI](https://www.spigotmc.org/resources/terminator-ai.95202/)
- [ ] [CookieClicker](https://www.spigotmc.org/resources/cookieclicker.65485/)
- [ ] [Herobrine](https://www.spigotmc.org/resources/herobrine.50393/)
- [ ] [Morph](https://www.spigotmc.org/resources/morph.8846/)

##### Ajouter des features

* [CrazyEnchantment](https://www.spigotmc.org/resources/crazy-enchantments.16470/)
* [MythicsMobs](https://www.spigotmc.org/resources/%E2%9A%94-mythicmobs-free-version-%E2%96%BAthe-1-custom-mob-creator%E2%97%84.5702/)
* [ExcellentEnchants](https://www.spigotmc.org/resources/excellentenchants-vanilla-like-enchantments-1-15-1-17.61693/)
* [MoarBows](https://www.spigotmc.org/resources/moar-bows.36387/)
* [InfernalMobs](https://www.spigotmc.org/resources/infernal-mobs.2156/)
* [KrakenMobCoins](https://www.spigotmc.org/resources/krakenmobcoins-1-8-1-17-the-most-feature-rich-mobcoins-plugin-with-decimals-support.88972/)
* [XP Boost](https://www.spigotmc.org/resources/xp-boost.13537/) => En tant qu'atout


#### Optimisation

#### Events

- [ ] Ajouter des plugins pour l'organisation des évènements
- [ ] Ajout des script pour la modification dynamique de certains plugins (ex: prix x2, kill x2 , etc ...)
- [ ] Réquilibrer les récompenses des évènements

#### Monitoring

- [ ] Mettre en place une application web pour monitorer le serveur en ligne (+ accès console)

#### Script

- [ ] Améliorer les commande Symfony en volumant le dossier paper
