stack_name = minecraft
paper_container_id = $(shell docker ps --filter name="$(stack_name)_paper" -q)

deploy:
	docker stack deploy -c docker-compose.yml $(stack_name)

undeploy:
	docker stack rm $(stack_name)

console:
	docker exec -it $(paper_container_id) rcon-cli

log:
	docker logs -f $(paper_container_id)

bash:
	docker exec -it $(paper_container_id) bash