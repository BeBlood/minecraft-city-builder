Minecraft Paper
===============

## Requirements

In order to use this project you'll need to install:

   - Docker: [official documentation](https://docs.docker.com/get-started/).
   - Make: [documentation](https://www.gnu.org/software/make/manual/make.html).

## How to install

1. Add the following line at the beginning of the /etc/hosts file :

```
127.0.0.1      adminer.minecraft-paper.docker nginx.minecraft-paper.docker grafana.minecraft-paper.docker
```

2. Open a terminal in the folder. If you do not have a reverse-proxy running on your computer, enter this :

```shell
$ make deploy-reverse-proxy
```

3. Enter the following lines :

```shell
$ make deploy [stack_name=*name you gave to your stack*]
$ make build-image [php_image_name=*name you gave to your php image*]
$ make composer-install
$ make yarn
$ make console command="doctrine:database:create"
$ make console command="doctrine:schema:update --force --dump-sql"
$ make encore
```

## Database configuration

You can add custom users in your database with the command (Username, Password and Roles can be changed :

```
$ make console cmd="app:security:create-user admin admin --role ROLE_ADMIN"
```