<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

class MultiplyEconomyShopGUIPricesCommand extends Command
{
    const TYPE_BUY = 'buy';
    const TYPE_SELL = 'sell';

    protected static $defaultName = 'app:economy-shop-guy:multiply-prices';

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate a worth.yml essential configuration file from EconomyShopGUI shops.yml file.')
            ->addOption('file-path', null, InputOption::VALUE_REQUIRED, 'The EconomyShopGUI shops.yml file path', '/var/www/html/src/Resources/shops.yml')
            ->addOption('output-file', 'o', InputOption::VALUE_REQUIRED, 'The output file path of repriced EconomyShopGUI shops.yml file', '/var/www/html/src/Resources/shops-updated.yml')
            ->addOption('type', null, InputOption::VALUE_REQUIRED, 'The type of price to multiple [buy, sell]', self::TYPE_SELL)
            ->addOption('factor', null, InputOption::VALUE_REQUIRED, 'The factor used to multiply price. Bondaries: ]0;+Inf]', 1)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (!in_array($input->getOption('type'), self::getSupportedTypes())) {
            $io->error(sprintf('The given type "%s" is not supported. Supported types %s', $input->getOption('type'), json_encode(self::getSupportedTypes())));
            
            return Command::FAILURE;
        }

        if ($input->getOption('factor') <= 0) {
            $io->error(sprintf('The factor must be grater than 0, "%s" given', $input->getOption('factor')));   
        }

        $io->info(sprintf('Parsing EconomyShopGUI shops.yml file (from %s) ...', $input->getOption('file-path')));

        try {
            $shops = Yaml::parseFile($input->getOption('file-path'));
            foreach ($shops as $category => &$items) {
                foreach ($items as &$item) {
                    if ('-1' != $item[$input->getOption('type')]) {
                        $item[$input->getOption('type')] *= $input->getOption('factor');
                    }
                }
            }

            $io->info(sprintf('Generating repriced EconomyShopGUI shops.yml file (in %s) ...', $input->getOption('output-file')));

            file_put_contents($input->getOption('output-file'), Yaml::dump($shops));
        } catch (\Exception $e) {
            $io->error(sprintf('An error as occured: %s', $e->getMessage()));

            return Command::FAILURE;
        }

        $io->success(sprintf('The file %s was successfully created !', $input->getOption('output-file')));

        return Command::SUCCESS;
    }

    public static function getSupportedTypes(): array
    {
        return [self::TYPE_BUY, self::TYPE_SELL];
    }
}
