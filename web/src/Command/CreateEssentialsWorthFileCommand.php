<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Yaml;

class CreateEssentialsWorthFileCommand extends Command
{
    protected static $defaultName = 'app:essential:create-worth-file';

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate a worth.yml Essentials configuration file from EconomyShopGUI shops.yml file.')
            ->addOption('file-path', null, InputOption::VALUE_REQUIRED, 'The EconomyShopGUI shops.yml file path', '/var/www/html/src/Resources/shops-updated.yml')
            ->addOption('output-file', 'o', InputOption::VALUE_REQUIRED, 'The output file path of worth.yml file', '/var/www/html/src/Resources/worth.yml')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->info(sprintf('Parsing EconomyShopGUI shops.yml file (from %s) ...', $input->getOption('file-path')));

        try {
            $worthItems = [];

            $shops = Yaml::parseFile($input->getOption('file-path'));
            foreach ($shops as $category => &$items) {
                foreach ($items as &$item) {
                    if ('-1' != $item['sell']) {
                        $worthItems[strtolower(str_replace('_', '', $item['material']))] = $item['sell'];
                    }
                }
            }

            ksort($worthItems);

            $io->info(sprintf('Generating Essentials worth.yml file (in %s) ...', $input->getOption('output-file')));

            file_put_contents($input->getOption('output-file'), Yaml::dump(['worth' => $worthItems]));
        } catch (\Exception $e) {
            $io->error(sprintf('An error as occured: %s', $e->getMessage()));

            return Command::FAILURE;
        }

        $io->success(sprintf('The file %s was successfully created !', $input->getOption('output-file')));

        return Command::SUCCESS;
    }
}
