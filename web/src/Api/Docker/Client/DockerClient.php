<?php

namespace App\Api\Docker\Client;

use App\Api\Docker\Model\Container;
use App\Api\Docker\Model\Service;
use App\Api\Docker\Model\Task;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class DockerClient
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://localhost',
        ]);
    }

    public function request($method, $url, $options = []): Response
    {
        $options['curl'] = [CURLOPT_UNIX_SOCKET_PATH => '/var/run/docker.sock'];

        return $this->client->request($method, $url, $options);
    }

    // General

    public function info(): array
    {
        $response = $this->request('GET', '/info');

        return json_decode($response->getBody()->getContents(), true);
    }

    // Services

    public function getServices()
    {
        // TODO
    }

    public function getService(string $id): Service
    {
        $response = $this->request('GET', sprintf('/services/%s', $id));

        $service = json_decode($response->getBody()->getContents(), true);

        return new Service(
            $this,
            $service['ID'],
            $service['Version'],
            new \DateTime($service['CreatedAt']),
            new \DateTime($service['UpdatedAt']),
            $service['Spec'],
            $service['Endpoint']
        );
    }

    // Tasks

    public function getTasks(array $query): array
    {
        $endpoint = '/tasks';

        if (!empty($query)) {
            $endpoint .= '?'.http_build_query($query);
        }

        try {
            $response = $this->request('GET', $endpoint, []);

            $tasks = json_decode($response->getBody()->getContents(), true);

            foreach ($tasks as &$task) {
                $task = new Task(
                    $this,
                    $task['ID'],
                    $task['Version'],
                    new \DateTime($task['CreatedAt']),
                    new \DateTime($task['UpdatedAt']),
                    $task['Labels'],
                    $task['Spec'],
                    $task['ServiceID'],
                    $task['Slot'],
                    $task['NodeID'],
                    $task['Status'],
                    $task['DesiredState'],
                    $task['NetworksAttachments']
                );
            }

            return $tasks;
        } catch (GuzzleException $e) {
            if (404 === $e->getCode()) {
                $text = sprintf('No such container: %s', $this->id);
                throw new ResourceNotFoundException($text, 404);
            }

            throw $e;
        }
    }

    public function getTask()
    {
        // TODO
    }

    // Containers

    public function getContainers(array $query): array
    {
        $endpoint = '/containers/json';

        if (!empty($query)) {
            $endpoint .= '?'.http_build_query($query);
        }

        try {
            $response = $this->request('GET', $endpoint, []);

            $containers = json_decode($response->getBody()->getContents(), true);

            foreach ($containers as &$container) {
                $container = new Container(
                    $this,
                    $container['Id'],
                    $container['Names'],
                    $container['Image'],
                    $container['ImageID'],
                    $container['Command'],
                    $container['Created'],
                    $container['Ports'],
                    $container['Labels'],
                    $container['State'],
                    $container['Status'],
                    $container['HostConfig'],
                    $container['NetworkSettings'],
                    $container['Mounts']
                );
            }

            return $containers;
        } catch (GuzzleException $e) {
            if (404 === $e->getCode()) {
                $text = sprintf('No such container: %s', $this->id);
                throw new ResourceNotFoundException($text, 404);
            }

            throw $e;
        }
    }

    public function getContainer()
    {
        // TODO
    }
}
