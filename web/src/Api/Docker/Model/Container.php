<?php

namespace App\Api\Docker\Model;

use App\Api\Docker\Client\DockerClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class Container
{
    private DockerClient $client;

    private string $id;

    private array $names;

    private string $image;

    private string $imageId;

    private string $command;

    private int $created;

    private array $ports;

    private array $labels;

    private string $state;

    private string $status;

    private array $hostConfig;

    private array $networkSettings;

    private array $mounts;

    public function __construct(
        DockerClient $client,
        string $id,
        array $names,
        string $image,
        string $imageId,
        string $command,
        int $created,
        array $ports,
        array $labels,
        string $state,
        string $status,
        array $hostConfig,
        array $networkSettings,
        array $mounts
    ) {
        $this->client = $client;
        $this->id = $id;
        $this->names = $names;
        $this->image = $image;
        $this->imageId = $imageId;
        $this->command = $command;
        $this->created = $created;
        $this->ports = $ports;
        $this->labels = $labels;
        $this->state = $state;
        $this->status = $status;
        $this->hostConfig = $hostConfig;
        $this->networkSettings = $networkSettings;
        $this->mounts = $mounts;
    }

    public function inspect(): array
    {
        $response = $this->client->request('GET', sprintf('/containers/%s/json', $this->id));

        return json_decode($response->getBody()->getContents(), true);
    }

    public function logs(array $query = []): string
    {
        $endpoint = sprintf('/containers/%s/logs', $this->id);

        $query['stdout'] = $query['stdout'] ?? 'true';
        $query['stderr'] = $query['stderr'] ?? 'true';

        if (!empty($query)) {
            $endpoint .= '?'.http_build_query($query);
        }

        try {
            $response = $this->client->request('GET', $endpoint, []);

            $text = $response->getBody()->getContents();

            $text = utf8_encode($text);
            $text = preg_replace('/(\\x00.|\\x01.|\\x02.)/', '', $text);
            $text = preg_replace('/\S\[/', '[', $text);

            return $text;
        } catch (GuzzleException $e) {
            if (404 === $e->getCode()) {
                $text = sprintf('No such container: %s', $this->id);
                throw new ResourceNotFoundException($text, 404);
            }

            throw $e;
        }
    }

    public function pause(): void
    {
        $this->client->request('POST', sprintf('/containers/%s/pause', $this->id));
    }

    public function unpause()
    {
        $this->client->request('POST', sprintf('/containers/%s/unpause', $this->id));
    }

    public function restart()
    {
        $this->client->request('POST', sprintf('/containers/%s/restart', $this->id));
    }

    public function exec(array $options)
    {
        $endpoint = sprintf('/containers/%s/exec', $this->id);

        try {
            $response = $this->client->request('POST', $endpoint, [
                'json' => $options,
            ]);

            $data = json_decode($response->getBody()->getContents(), true);

            return new Exec($this->client, $data['Id']);
        } catch (GuzzleException $e) {
            if (404 === $e->getCode()) {
                $text = sprintf('No such container: %s', $this->id);
                throw new ResourceNotFoundException($text, 404);
            }

            throw $e;
        }
    }
}
