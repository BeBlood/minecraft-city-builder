<?php

namespace App\Api\Docker\Model;

use App\Api\Docker\Client\DockerClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class Exec
{
    private DockerClient $client;

    private string $id;

    public function __construct(
        DockerClient $client,
        string $id
    ) {
        $this->client = $client;
        $this->id = $id;
    }

    public function start(): string
    {
        try {
            $response = $this->client->request('POST', sprintf('/exec/%s/start', $this->id), [
                'json' => [
                    'Detach' => false,
                    'Tty' => false,
                ],
            ]);

            return $response->getBody()->getContents();
        } catch (GuzzleException $e) {
            if (404 === $e->getCode()) {
                $text = sprintf('No such exec: %s', $this->id);
                throw new ResourceNotFoundException($text, 404);
            }

            throw $e;
        }
    }
}
