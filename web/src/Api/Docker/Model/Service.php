<?php

namespace App\Api\Docker\Model;

use App\Api\Docker\Client\DockerClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class Service
{
    private DockerClient $client;

    private string $id;

    private array $version;

    private \DateTimeInterface $createdAt;

    private \DateTimeInterface $updatedAt;

    private array $spec;

    private array $endpoint;

    public function __construct(
        DockerClient $client,
        string $id,
        array $version,
        \DateTimeInterface $createdAt,
        \DateTimeInterface $updatedAt,
        array $spec,
        array $endpoint
    ) {
        $this->client = $client;
        $this->id = $id;
        $this->version = $version;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->spec = $spec;
        $this->endpoint = $endpoint;
    }

    public function inspect()
    {
        // TODO
    }

    public function logs(array $query = []): string
    {
        $endpoint = sprintf('/services/%s/logs', $this->id);

        $query['stdout'] = $query['stdout'] ?? 'true';
        $query['stderr'] = $query['stderr'] ?? 'true';

        if (!empty($query)) {
            $endpoint .= '?'.http_build_query($query);
        }

        try {
            $response = $this->client->request('GET', $endpoint, []);

            $text = $response->getBody()->getContents();
            $text = utf8_encode($text);

            return $text;
        } catch (GuzzleException $e) {
            if (404 === $e->getCode()) {
                $text = sprintf('No such container: %s', $this->id);
                throw new ResourceNotFoundException($text, 404);
            }

            throw $e;
        }
    }

    public function tasks(array $query = []): array
    {
        $query['service'] = [$this->id];

        return $this->client->getTasks([
            'filters' => json_encode($query),
        ]);
    }
}
