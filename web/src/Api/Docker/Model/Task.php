<?php

namespace App\Api\Docker\Model;

use App\Api\Docker\Client\DockerClient;

class Task
{
    const DESIRED_STATE_RUNNING = 'running';
    const DESIRED_STATE_SHUTDOWN = 'shutdown';
    const DESIRED_STATE_ACCEPTED = 'accepted';

    private DockerClient $client;

    private string $id;

    private array $version;

    private \DateTimeInterface $createdAt;

    private \DateTimeInterface $updatedAt;

    private array $labels;

    private array $spec;

    private string $serviceId;

    private int $slot;

    private string $nodeId;

    private array $status;

    private string $desiredState;

    private array $networksAttachments;

    public function __construct(
        DockerClient $client,
        string $id,
        array $version,
        \DateTimeInterface $createdAt,
        \DateTimeInterface $updatedAt,
        array $labels,
        array $spec,
        string $serviceId,
        int $slot,
        string $nodeId,
        array $status,
        string $desiredState,
        array $networksAttachments
    ) {
        $this->client = $client;
        $this->id = $id;
        $this->version = $version;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->labels = $labels;
        $this->spec = $spec;
        $this->serviceId = $serviceId;
        $this->slot = $slot;
        $this->nodeId = $nodeId;
        $this->status = $status;
        $this->desiredState = $desiredState;
        $this->networksAttachments = $networksAttachments;
    }

    public function inspect()
    {
        // TODO
    }

    public function logs()
    {
        // TODO
    }

    public function container()
    {
        //TODO
    }
}
