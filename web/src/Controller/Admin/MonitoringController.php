<?php

namespace App\Controller\Admin;

use App\Api\Docker\Client\DockerClient;
use App\Api\Docker\Model\Container;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/monitoring", name="monitoring_")
 */
class MonitoringController extends AbstractController
{
    private DockerClient $client;

    public function __construct()
    {
        $this->client = new DockerClient();
    }

    /**
     * @Route("/show", name="show", methods={"GET"})
     */
    public function show()
    {
        return $this->render('admin/monitoring/show.html.twig');
    }

    /**
     * @Route("/inspect", name="inspect", methods={"GET"})
     */
    public function inspect(Request $request)
    {
        $container = $this->getContainer()->inspect();

        return new JsonResponse($container);
    }

    /**
     * @Route("/logs.txt", name="logs", methods={"GET"})
     */
    public function logs(Request $request)
    {
        $logs = $this->getContainer()->logs([
            'tail' => $request->query->get('tail', 'all'),
        ]);

        if ($request->isXmlHttpRequest()) {
            return new Response(preg_replace('/,dfosndfosndfons/', '', $logs));
        }

        return $this->render('admin/monitoring/logs.html.twig');
    }

    /**
     * @Route("/play", name="play", methods={"POST"})
     */
    public function play(Request $request)
    {
        try {
            $this->getContainer()->unpause();
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()]);
        }

        return new JsonResponse(['message' => 'OK']);
    }

    /**
     * @Route("/pause", name="pause", methods={"POST"})
     */
    public function pause(Request $request)
    {
        try {
            $this->getContainer()->pause();
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()]);
        }

        return new JsonResponse(['message' => 'OK']);
    }

    /**
     * @Route("/restart", name="restart", methods={"POST"})
     */
    public function restart(Request $request)
    {
        try {
            $this->getContainer()->restart();
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()]);
        }

        return new JsonResponse(['message' => 'OK']);
    }

    /**
     * @Route("/exec", name="exec", methods={"POST"})
     */
    public function exec(Request $request)
    {
        try {
            $result = $this->getContainer()->exec([
                'Cmd' => ['bash', '-c', sprintf('rcon-cli %s', $request->request->get('cmd'))],
            ])->start();
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()]);
        }

        return new JsonResponse(['message' => 'OK', 'result' => $result]);
    }

    private function getContainer(): Container
    {
        $containers = $this->client->getContainers([
            'filters' => json_encode(['name' => ['minecraft_paper']]),
        ]);

        return $containers[0];
    }
}
