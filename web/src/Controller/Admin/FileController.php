<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/file", name="file_")
 */
class FileController extends AbstractController
{
    private string $basePath;

    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * @Route("/show/{_path}", name="show", methods={"GET"}, requirements={"_path"=".*"})
     */
    public function show(Request $request, string $_path)
    {
        $content = file_get_contents(sprintf('%s/%s', $this->basePath, $_path));

        return $this->render('admin/file/show.html.twig', [
            'path' => $_path,
            'content' => $content,
            'parent_path' => substr($_path, 0, strrpos($_path, '/') - strlen($_path)),
        ]);
    }

    /**
     * @Route("/edit/{_path}", name="edit", methods={"GET", "POST"}, requirements={"_path"=".*"})
     */
    public function edit(Request $request, string $_path)
    {
        $filePath = sprintf('%s/%s', $this->basePath, $_path);

        if (Request::METHOD_POST === $request->getMethod() && $request->isXmlHttpRequest()) {
            try {
                $data = json_decode($request->getContent(), true);

                file_put_contents($filePath, $data['content']);
            } catch (\Exception $e) {
                return new JsonResponse(['message' => $e->getMessage()]);
            }

            return new JsonResponse(['message' => 'ok']);
        }

        $content = file_get_contents($filePath);

        return $this->render('admin/file/edit.html.twig', [
            'path' => $_path,
            'content' => $content,
            'parent_path' => substr($_path, 0, strrpos($_path, '/') - strlen($_path)),
        ]);
    }

    /**
     * @Route("/download/{_path}", name="download", methods={"GET"}, requirements={"_path"=".*"})
     */
    public function download(Request $request, string $_path)
    {
        $filePath = sprintf('%s/%s', $this->basePath, $_path);

        $response = new BinaryFileResponse($filePath);
        $response->headers->set('Content-Type', 'text/plain');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, substr($_path, 1 + strrpos($_path, '/') - strlen($_path)));

        return $response;
    }

    /**
     * @Route("/{_path?}", name="list", methods={"GET"}, requirements={"_path"=".*"})
     */
    public function list(Request $request, string $_path = null)
    {
        $filePath = sprintf('%s/%s', $this->basePath, $_path);

        $finder = new Finder();
        $finder
            ->files()
            ->in($filePath)
            ->depth('== 0')
            ->sortByName()
        ;

        $files = [];
        foreach ($finder as $file) {
            $files[] = $file;
        }

        $finder->directories();

        $directories = [];
        foreach ($finder as $directory) {
            $directories[] = $directory;
        }

        return $this->render('admin/file/list.html.twig', [
            'path' => $_path,
            'directories' => $directories,
            'files' => $files,
            'parent_path' => substr($_path, 0, strrpos($_path, '/') - strlen($_path)),
        ]);
    }

    /**
     * @Route("/{_path}", name="rename", methods={"PUT"}, requirements={"_path"=".*"})
     */
    public function rename(Request $request, string $_path)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestException('The request must be XmlHttpRequest');
        }

        $filePath = sprintf('%s/%s', $this->basePath, $_path);

        try {
            $file = new \SplFileInfo($filePath);
            $data = json_decode($request->getContent(), true);

            $newFilePath = str_replace($file->getFilename(), $data['filename'], $file->getPathname());

            (new Filesystem())->rename($filePath, $newFilePath);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()]);
        }

        return new JsonResponse(['message' => 'ok']);
    }

    /**
     * @Route("/{_path}", name="delete", methods={"DELETE"}, requirements={"_path"=".*"})
     */
    public function delete(Request $request, string $_path)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestException('The request must be XmlHttpRequest');
        }

        $filePath = sprintf('%s/%s', $this->basePath, $_path);

        try {
            (new Filesystem())->remove($filePath);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()]);
        }

        return new JsonResponse(['message' => 'ok']);
    }

    /**
     * @Route("/{_path?}", name="upload", methods={"POST"}, requirements={"_path"=".*"})
     */
    public function upload(Request $request, string $_path = null)
    {
        $filePath = sprintf('%s/%s', $this->basePath, $_path);

        try {
            foreach ($request->files->all() as $file) {
                $file->move($filePath, $file->getClientOriginalName());
            }
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()]);
        }

        return new JsonResponse(['message' => 'ok']);
    }
}
