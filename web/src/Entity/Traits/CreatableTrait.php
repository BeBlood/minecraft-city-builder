<?php

namespace App\Entity\Traits;

trait CreatableTrait
{
    private ?\DateTimeInterface $createdAt = null;

    public function onPrePersist()
    {
        if (null !== $this->getCreatedAt()) {
            return;
        }
        
        $this->setCreatedAt(new \DateTime());
        if ((new \ReflectionClass($this))->hasMethod('setUpdatedAt')) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
