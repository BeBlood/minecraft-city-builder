<?php

namespace App\Entity\Traits;

trait UpdatableTrait
{
    private ?\DateTimeInterface $updatedAt = null;

    public function onPreUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
