require('./scss/admin.scss');

const $ = require('jquery');
window.$ = global.$ = global.jQuery = $;
require('bootstrap/dist/js/bootstrap.js');

require('prismjs/prism');
require('prismjs/components/prism-log');

window.toastr = require('toastr');

window.moment = require('moment');

window.Swal = require('sweetalert2');

window.BootstrapMenu = require('bootstrap-menu');

// Custom

require('./admin/file/edit');
require('./admin/file/list');
require('./admin/monitoring/logs');
require('./admin/monitoring/show');
