import deleteFile from './delete-file';

export default function (url, callback) {
    Swal.fire({
        title: 'Êtes vous sûr ?',
        text: "Vous ne pourrez pas revenir en arrière !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Valider',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.isConfirmed) {
          deleteFile(url, callback);
        }
    })
}