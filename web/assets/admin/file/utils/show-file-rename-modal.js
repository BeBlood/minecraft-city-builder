import renameFile from './rename-file';

export default function (url, callback) {
    Swal.fire({
        title: 'Quel est le nouveau nom du fichier/dossier ?',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'OK'
    }).then((result) => {
        if (result.isConfirmed) {
            renameFile(url, result.value, callback)
        }
    })
}