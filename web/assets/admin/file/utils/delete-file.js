export default function (url, callback) {
    fetch(url, {
        method: 'DELETE',
        headers: {
            "Content-type": "application/json",
            'X-Requested-With': 'XMLHttpRequest'
        }
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if ('ok' !== data.message) {
            return Promise.reject(data.message);
        }

        toastr.success('Fichier supprimé !')
        if (typeof callback === 'function') {
            callback();
        }
    })
    .catch(function (error) {
        toastr.error('Une erreur est survenu: ' + error);
    })
}