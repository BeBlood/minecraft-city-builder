export default function (files, url, callback) {
    var data = new FormData();

    files.forEach(file => {
        data.append(file.name, file);
    });

    fetch(url, {
        method: 'POST',
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: data
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if ('ok' !== data.message) {
            return Promise.reject(data.message);
        }

        toastr.success('Fichier ajouté !');
        if (typeof callback === 'function') {
            callback();
        }
    })
    .catch(function (error) {
        toastr.error('Une erreur est survenu: ' + error);
    })
}