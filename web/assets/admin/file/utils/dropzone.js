import uploadFiles from './upload-files';

function setDark(element) {
    element.style.filter = 'opacity(0.7)';
};

function setLight(element) {
    element.style.filter = 'opacity(1)';
};

export default function (selector) {
    var dropzone = document.querySelector(selector);

    if (null === dropzone) {
        return;
    }

    dropzone.addEventListener('dragleave', function () {
        setLight(this);
    }, false);

    dropzone.addEventListener('dragover', function (event) {
        event.preventDefault();

        setDark(this);
    }, false);


    dropzone.addEventListener("drop", function (event) {
        event.preventDefault();
        setLight(this);

        let files = [];
        if (event.dataTransfer.items) {
            for (let i = 0; i < event.dataTransfer.items.length; i++) {
              if (event.dataTransfer.items[i].kind === 'file') {
                files[i] = event.dataTransfer.items[i].getAsFile();
              }
            }
        } else {
            for (let i = 0; i < event.dataTransfer.files.length; i++) {
                files[i] = event.dataTransfer.files[i];
            }
        }

        uploadFiles(files, this.getAttribute('data-file-upload-url'), function () {
            location.reload();
        });
    }, false);
}