export default function (url, filename, callback) {
    fetch(url, {
        method: 'PUT',
        headers: {
            "Content-type": "application/json",
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: JSON.stringify({filename: filename})
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if ('ok' !== data.message) {
            return Promise.reject(data.message);
        }

        toastr.success('Fichier renommé !');
        if (typeof callback === 'function') {
            callback();
        }
    })
    .catch(function (error) {
        toastr.error('Une erreur est survenu: ' + error);
    })
}