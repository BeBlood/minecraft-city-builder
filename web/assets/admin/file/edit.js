import showFileDeleteModal from './utils/show-file-delete-modal';
import showFileRenameModal from './utils/show-file-rename-modal';

document.addEventListener('DOMContentLoaded', function () {
    let deleteFileButton = document.querySelector('#delete-file-button');
    if (null !== deleteFileButton) {
        deleteFileButton.addEventListener('click', function () {
            showFileRenameModal(this.getAttribute('data-file-rename-url'), () => {
                window.location = this.getAttribute('data-callback-location-url');
            })
        })
    }

    let renameFileButton = document.querySelector('#rename-file-button')
    if (null !== renameFileButton) {
        renameFileButton.addEventListener('click', function () {
            showFileRenameModal(this.getAttribute('data-file-rename-url'), () => {
                window.location = this.getAttribute('data-callback-location-url');
            })
        });
    }
});