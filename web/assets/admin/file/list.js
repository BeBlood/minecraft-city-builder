var moment = require('moment');
var toastr = require('toastr');

import showFileDeleteModal from './utils/show-file-delete-modal';
import showFileRenameModal from './utils/show-file-rename-modal';
import getHumanReadableFileSize from './utils/get-human-readable-file-size';
import dropzone from './utils/dropzone';

document.addEventListener('DOMContentLoaded', function () {
    // Show file last modified
    document.querySelectorAll('.file-last-modified').forEach(function (element) {
        element.innerHTML = moment(element.innerHTML, "X").fromNow();
    });

    // Show human readable file size
    document.querySelectorAll('.file-size').forEach(function (element) {
        element.innerHTML = getHumanReadableFileSize(element.innerHTML, true);
    });

    // Add context menu
    var menu = new BootstrapMenu('.directory li', {
        fetchElementData: function(row) {
            return row.data();
        },
        actions: [
            {
                name: 'Renommer',
                iconClass: 'bi bi-type',
                onClick: function(data) {
                    showFileRenameModal(data.fileRenameUrl, function () {
                        location.reload();
                    });
                }
            },
            {
                name: 'Supprimer',
                iconClass: 'bi bi-trash',
                onClick: function(data) {
                    showFileDeleteModal(data.fileDeleteUrl, function () {
                        location.reload();
                    });
                }
            }
        ]
    });

    // Add dropzone
    dropzone('.directory');
})
