export default function showLogs(element) {
    let url = element.getAttribute('data-logs-url') + '?tail=' + element.getAttribute('data-logs-tail');
    fetch(url, {
        headers: {'X-Requested-With': 'XMLHttpRequest'}
    })
    .then(function(response) {
        if (!response.ok) {
            return;
        }

        return response.text();
    })
    .then(function(logs) {
        element.innerHTML = Prism.highlight(logs, Prism.languages.log, 'log');
    });
}