import showLogs from './show-logs';

export default function exec(element, callback) {
    var data = new FormData();

    data.append('cmd', element.value);

    fetch(element.getAttribute('data-exec-url'), {
        method: 'POST',
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: data
    })
    .then(function (response) {
        return response.json();
    })
    .then(function(data) {
        if ('OK' !== data.message) {
            return Promise.reject(data.message);
        }

        element.value = '';

        if (null !== element.getAttribute('data-logs-container')) {
            showLogs(document.querySelector(element.getAttribute('data-logs-container')));
        }

        if (typeof callback === 'function') {
            callback();
        }
    })
    .catch(function (error) {
        toastr.error('Une erreur est survenu: ' + error);
    });
}