import exec from './utils/exec';

document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('.container-exec').forEach((element) => {
        element.addEventListener('keyup', function (event) {
            if (event.keyCode === 13) {
                exec(this);
            }
        })
    });
})