import showLogs from './utils/show-logs';

document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('[data-logs-url]').forEach((element) => {
        showLogs(element);

        setInterval(() => {
            showLogs(element);
        }, 10000);
    });
})