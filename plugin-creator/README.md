# Plugin Creator

Ce mini-projet a pour but de simplifier la création et/ou modification d'un plugin Minecraft (pré-configuré pour 1.17.1).

## Prérequis

Vous devez créer l'image docker utilisée pour la compilation du plugin (Java 17 + Ant):

```shell
$ make build-image
```

## Utilisation

1 - Placer les sources du plugin dans `src/`.

2 - Modifier les propriétés du fichier `src/plugin.yml`:

```yaml
name: MyPlugin
author: BeBlood
version: 1.0
description: My description.
main: fr.beblood.MyPlugin.Main # Your main class
```

3 - Télécharger l'API de Spigot (ou l'ensemble des dépendences maven):

```shell
$ make spigot-api [spigot-api-version="1.17.1-R0.1-SNAPSHOT"]
```

Dans le cas où d'autres dépendences sont néccessaires et qu'un fichier `pom.xml` est présent pour le plugin, replacer le fichier existant et installer les dépendences supplémentaires avec:

```shell
$ make lib
```

4 - Compiler les sources du plugin:

```shell
$ make build [jar_name="MyPlugin"]
```

Attention si le projet a déjà été compilé une première fois, il faut clean le précédent build, sinon la commande ci-dessus n'aura aucun effet:

```shell
$ make clean
```

## Decompilation

Pour décompiler un plugin, placer le jar file dans le dossier `decompile/in/` puis utilisez la commande suivante:

```shell
$ make decompile
```

Les version décompilées seront accessibles depuis le dossier `decompile/out/`.