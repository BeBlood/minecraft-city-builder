# Créer un PNJ shop

```
# Créer le PNJ
/npc create [NAME]

# Sélectionner un PNJ
/npc list
/npc select [ID/NAME]
/npc remove [ID/NAME]

# Renomer un PNJ
/npc rename [NAME]

# Modifier l'apparence du PNJ
/npc skin [PLAYER_NAME]
/npc skin --url https://mineskin.org/gallery/1
/npc size 2
/npc equip

# Repositioner le PNJ
/npc tphere

# Ajouter un shop à un PNJ
/npc command add shop [SECTION] <p>
```

# Editer le shop

Pour configurer les sections: 

```
plugins/EconomyShopGUI/sections.yml
```

Pour ce qui est afficher dans le /show [SECTION]

```
plugins/EconomyShopGUI/shops.yml
```

# Commandes préparées

```

# Blocks

/npc create Blocks
/npc command add shop Blocks <p>

# Decoration

/npc create Decoration
/npc command add shop Decoration <p>

# Dyes

/npc create Dyes
/npc command add shop Dyes <p>

# Enchanting

/npc create Enchanting
/npc command add shop Enchanting <p>

# Farming

/npc create Farming
/npc command add shop Farming <p>

# Food

/npc create Food
/npc command add shop Food <p>

# Mobs

/npc create Mobs
/npc command add shop Mobs <p>

# Music

/npc create Music
/npc command add shop Music <p>

# Ores

/npc create Ores
/npc command add shop Ores <p>

# Others

/npc create Others
/npc command add shop Others <p>

# Potions

/npc create Potions
/npc command add shop Potions <p>

# Redstone

/npc create Redstone
/npc command add shop Redstone <p>

# Spawners

/npc create Spawners
/npc command add shop Spawners <p>

# Workstations

/npc create Workstations
/npc command add shop Workstations <p>
```

# Credits

* https://wiki.citizensnpcs.co/Commands
* https://gpplugins.gitbook.io/economyshopgui/basics/how-to#how-to-make-an-npc-shop